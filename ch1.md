Chapter 1: Overview
================
Daniel Spracklin
September 20, 2020

``` r
library(tidyverse)
library(magrittr)
library(scales)
library(arm)
library(rstanarm)
library(bayesplot)
library(daniel)

daniel::set_up_fonts()
```

## Sections 1.1 and 1.2

Gelman *et al.* frame statistical inference as an exercise in
*generalizing*: from sample to population; from treatment to control;
and from observations to underlying constructs. In order to make good
inference, they suggest four key applications for regression:
**predicting**; exploring **associations** between variables;
**extrapolating** from a sample to a population; and making **causal
inference** about treatment effects.

On a technical level, the authors use `rstanarm::stan_glm()` to fit
linear models. In the `hibbs` example, the results are virtually
identical to those reached with either `stats::lm()`, although the
authors note in Section 1.6 that given strong priors, `stan_glm()` and
`lm()` will return more different results. Note also that we could
quickly fit the linear model with `ggplot2::geom_smooth(method = "lm",
se = F)` instead of extracting the intercept and slope ourselves, but
the point of this chapter is to fit and interact with models. In later
chapters, I may consider using `broom` to tidy regression results and
append them to an existing dataframe.

We can also set `algorithm = "optimizing"` in `stan_glm()` to speed up
the model-fitting process, as the expense of some precision.

``` r
hibbs <- read.table("https://raw.githubusercontent.com/avehtari/ROS-Examples/master/ElectionsEconomy/data/hibbs.dat",
                    header = T)

model <- stan_glm(vote ~ growth, data = hibbs, refresh = 0)
print(model)
```

    ## stan_glm
    ##  family:       gaussian [identity]
    ##  formula:      vote ~ growth
    ##  observations: 16
    ##  predictors:   2
    ## ------
    ##             Median MAD_SD
    ## (Intercept) 46.2    1.7  
    ## growth       3.1    0.7  
    ## 
    ## Auxiliary parameter(s):
    ##       Median MAD_SD
    ## sigma 3.9    0.7   
    ## 
    ## ------
    ## * For help interpreting the printed output see ?print.stanreg
    ## * For info on the priors used see ?prior_summary.stanreg

``` r
hibbs %>% 
  ggplot(aes(growth, vote)) +
  geom_point() +
  geom_hline(aes(yintercept = 50), colour = "gray50", linetype = "dashed") +
  geom_abline(aes(intercept = model$coefficients["(Intercept)"],
                  slope = model$coefficients["growth"])) +
  scale_x_continuous(label = function(x) {glue::glue("{x}%")}) +
  scale_y_continuous(label = function(x) {glue::glue("{x}%")}) +
  # geom_smooth(method = "lm", se = F) +
  theme_daniel() +
  labs(x = "Average recent growth in personal income",
       y = "Incumbent party's vote share")
```

![](ch1_files/figure-gfm/hibbs-1.png)<!-- -->

## Section 1.4

To illustrate the value of regression in causal inference, the authors
give the following example, which I’ve reproduced with ggplot2 using the
authors’ initial seed:

``` r
set.seed(1151)

N <- 50
x <- rnorm(N, 0, 1)^2
z <- rep(0:1, N/2)
x <- ifelse(z == 0, rnorm(N, 0, 1.2)^2, rnorm(N, 0, .8)^2)
y <- rnorm(N, 20 + 5 * x + 10 * z, 3)
data <- data.frame(x, z, y)

model <- lm(y ~ x + z, data = data)
arm::display(model, digits = 2)
```

    ## lm(formula = y ~ x + z, data = data)
    ##             coef.est coef.se
    ## (Intercept) 20.74     0.73  
    ## x            4.65     0.29  
    ## z           10.02     0.87  
    ## ---
    ## n = 50, k = 3
    ## residual sd = 2.92, R-Squared = 0.87

``` r
intercept <- model$coefficients["(Intercept)"] + z * model$coefficients["z"]
slope <- model$coefficients["x"]

data %>% 
  ggplot(aes(x, y, colour = as.character(z))) +
  geom_abline(aes(intercept = intercept, slope = slope, colour = as.character(z)),
              show.legend = F) +
  # geom_smooth(method = "lm", se = F) +
  geom_point() +
  scale_colour_daniel(name = "Type", labels = c("Control", "Treated")) +
  theme_daniel()
```

![](ch1_files/figure-gfm/simplecausal-1.png)<!-- -->

The authors advise against using models with large standard errors as
forecasting tools. On the other hand, such models are useful for
exploring associations between variables.

The authors give a four-step method for regression:

1.  **Build** and expand (via predictors, interactions and
    transformations) a linear model.
2.  **Fit** the model by estimating coefficients and uncertainties.
3.  **Understand** the fit, using graphs and connections between
    parameters and the underlying constructs of interest.
4.  **Criticize** the model, which is a way to improve models, or read
    down naive findings about a model.

## Section 1.5

The authors note that despite the differences between classical and
Bayesian inference, the two streams are united by the **information**
used, the **assumptions** made and the **interpretation** of estimates.

  - **Information**: we usually have information about the data that was
    observed, how it was collected, and any prior knowledge we may have.
    However, take care not to overstate the prior knowledge, since it
    may be inflated in the literature.
  - **Assumptions**: we typically assume a type of relationship between
    ![x](https://latex.codecogs.com/png.latex?x "x") and
    ![y](https://latex.codecogs.com/png.latex?y "y") (linear,
    transformed to linear, etc.). We also make assumptions about where
    the data came from (sampling, surveying, etc.), and about the
    real-world relevance of the measured data.

The **classical inference**, frequentist model seeks low-bias,
low-variance, “pure” statistical summaries that behave well in the long
run. This is the principle of *conservatism*: even when the data are
weak, we’d like to make some approximation about the bias of our
estimates and the coverage of our intervals. The downside of this
approach is when data are scarce, indirect, or highly variable: we may
not be able to learn much from a model, as its confidence intervals may
be inflated, especially when compared to the prior knowledge.

The **Bayesian inference** model goes beyond summary to include prior
information. This can give more reasonable results and can allow for
more direct predictions about the future. However, we must have prior
knowledge (and indeed prior knowledge that others can agree on). One
practical idea to solve this is to simulate in order to better summarize
uncertainty.

## Section 1.8

  - **Question 1.2**. We return to the `hibbs` example, but now show
    hypothetical data corresponding to ![y = 30\\
    \\verb\!+\!\\ 10x](https://latex.codecogs.com/png.latex?y%20%3D%2030%5C%20%5Cverb%21%2B%21%5C%2010x
    "y = 30\\ \\verb!+!\\ 10x") with residual standard deviations 3.9
    and 10. We’re only interested in sketching the result, so we’ll
    quickly jitter each point from the given line with `rnorm`, setting
    the standard deviation of the jitter to be the desired residual
    standard deviation. As expected, we see more uncertainty in the
    `geom_smooth()` confidence intervals when the residual standard
    deviation is higher.

<!-- end list -->

``` r
set.seed(613)

jitter_data <- function(df, sd) {
  df %>%
    rowwise() %>%
    mutate(vote = 30 + (10 * growth) + rnorm(1, mean = 0, sd = sd),
           vote_type = glue::glue('Residual SD near {sd}'))
}

hibbs1 <- c(3.9, 10) %>% 
  purrr::map_dfr(~jitter_data(hibbs, .x))

hibbs1 %>%
  group_by(vote_type) %>% 
  nest() %>%
  mutate(models = purrr::map(data, ~stan_glm(vote ~ growth, data = ., refresh = 0))) %>% 
  pull(models)
```

    ## [[1]]
    ## stan_glm
    ##  family:       gaussian [identity]
    ##  formula:      vote ~ growth
    ##  observations: 16
    ##  predictors:   2
    ## ------
    ##             Median MAD_SD
    ## (Intercept) 32.6    1.9  
    ## growth       9.1    0.8  
    ## 
    ## Auxiliary parameter(s):
    ##       Median MAD_SD
    ## sigma 4.5    0.9   
    ## 
    ## ------
    ## * For help interpreting the printed output see ?print.stanreg
    ## * For info on the priors used see ?prior_summary.stanreg
    ## 
    ## [[2]]
    ## stan_glm
    ##  family:       gaussian [identity]
    ##  formula:      vote ~ growth
    ##  observations: 16
    ##  predictors:   2
    ## ------
    ##             Median MAD_SD
    ## (Intercept) 27.8    5.1  
    ## growth       9.0    2.2  
    ## 
    ## Auxiliary parameter(s):
    ##       Median MAD_SD
    ## sigma 12.3    2.3  
    ## 
    ## ------
    ## * For help interpreting the printed output see ?print.stanreg
    ## * For info on the priors used see ?prior_summary.stanreg

``` r
hibbs1 %>% 
  ggplot(aes(growth, vote, colour = vote_type)) +
  geom_point() +
  geom_hline(aes(yintercept = 50), colour = "gray50", linetype = "dashed") +
  scale_x_continuous(label = function(x) {glue::glue("{x}%")}) +
  scale_y_continuous(label = function(x) {glue::glue("{x}%")}) +
  scale_colour_daniel(name = NULL) +
  scale_fill_daniel(name = NULL) +
  geom_smooth(aes(fill = vote_type), method = "lm", se = T) +
  theme_daniel() +
  labs(x = "Average recent growth in personal income",
       y = "Incumbent party's vote share")
```

![](ch1_files/figure-gfm/hibbs%20redux-1.png)<!-- -->
