Chapter 2: Data and measurement
================
Daniel Spracklin
October 2, 2020

``` r
library(tidyverse)
library(magrittr)
library(scales)
library(arm)
library(rstanarm)
library(bayesplot)
library(rlang)
library(purrr)
library(daniel)
library(patchwork)

daniel::set_up_fonts()
```

## Section 2.1

The authors give the example of a plotted map that showed unintuitive
trends in states’ Human Development Index (HDI). Here is a reproduction
of the data:

``` r
hdi <- read.table("https://raw.githubusercontent.com/avehtari/ROS-Examples/master/HDI/data/hdi.dat",
                    header = T)

hdi %>% 
  ggplot(aes(reorder(state, rank), hdi)) +
  geom_point() +
  scale_x_discrete(breaks = NULL) +
  theme_daniel() +
  theme(axis.text.x = element_blank()) +
  labs(x = "State",
       y = "HDI")
```

![](ch2_files/figure-gfm/HDI-1.png)<!-- -->

By decomposing the data into its constituent parts (life expectancy,
education and standard of living) and plotting these parts, the authors
better understood the data.

The authors also give an example from Pew data to show that even similar
measures, like political ideology and American political party
identification, can answer different questions.

``` r
pew <- foreign::read.dta("https://raw.githubusercontent.com/avehtari/ROS-Examples/master/Pew/data/pew_research_center_june_elect_wknd_data.dta") %>% 
  dplyr::select(party, partyln, income, ideo) %>% 
  mutate(party = as.numeric(party),
         partyln = as.numeric(partyln),
         lean = case_when(party == 2   ~ "Republican",
                          party == 3   ~ "Democrat",
                          partyln == 2 ~ "Lean Republican",
                          partyln == 4 ~ "Lean Democrat",
                          T            ~ "Independent"),
         lean = factor(lean, levels = c("Republican", "Lean Republican",
                                        "Independent", "Lean Democrat", "Democrat")),
         ideo = as.numeric(ideo),
         ideo = case_when(ideo == 2 ~ "Very conservative",
                          ideo == 3 ~ "Conservative",
                          ideo == 6 ~ "Very liberal",
                          ideo == 5 ~ "Liberal",
                          T         ~ "Moderate"),
         ideo = factor(ideo, levels = c("Very conservative", "Conservative",
                                        "Moderate", "Liberal", "Very liberal")),
         income = as.character(income)) %>%
  filter(income != "dk/refused") %>% 
  mutate(income = factor(as.character(income),
                         levels = c("less than $10,000", "$10,000-$19,999",
                                    "$20,000-$29,999", "$30,000-$39,999",
                                    "$40,000-$49,000", "$50,000-$74,999",
                                    "$75,000-$99,999", "$100,000-$149,999",
                                    "$150,000+"),
                         labels = c("less than $10k", "$10k-$20k",
                                    "$20k-$30k", "$30k-$40k",
                                    "$40k-$50k", "$50k-$75k",
                                    "$75k-$100k", "$100k-$150k",
                                    "more than $150k"))) %>% 
  filter(!is.na(income))

center_label <- function(df, x_var) {
  
  top_level <- max(as.integer(df[[ensym(x_var)]]))
  top_level <- if (top_level %% 2 == 0) {
    top_level / 2} else {
    (top_level + 1) / 2}

  df %>%
    arrange({{x_var}}) %>%
    group_by({{x_var}}) %>%
    mutate(factor_column = as.integer({{x_var}}),
           label_top = case_when(factor_column != top_level ~ NA_real_,
                                 T ~ cumsum(n) / sum(n)),
           label_center = case_when(is.na(lag(label_top, 1)) ~ label_top / 2,
                                    T ~ (label_top + lag(label_top, 1)) / 2 )) %>%
    dplyr::select(-label_top)
}

grapher <- function(df, x_var, group_var, title) {
  
  group_var <- sym(group_var)
  
  p <- df %>% 
    count({{x_var}}, !!group_var) %>% 
    center_label(., {{x_var}}) %>% 
    ggplot() +
    geom_area(aes({{x_var}}, .data$n, group = !!group_var,
                  colour = !!group_var, fill = !!group_var),
              position = "fill") +
    geom_text(aes({{x_var}}, abs(1 - .data$label_center),
                  group = !!group_var, label = !!group_var)) +
    scale_x_discrete(labels = c("Low income", rep("", 3), "Middle income",
                                rep("", 3), "High income")) +
    scale_y_continuous(label = scales::percent) +
    scale_colour_daniel(name = NULL, palette = "main") +
    scale_fill_daniel(name = NULL, palette = "main") +
    theme_daniel() +
    guides(colour = F, fill = F) +
    labs(title = glue::glue("Income and {title}, 2008"),
         x = NULL,
         y = NULL)

  print(p)
}

list(c("ideo", "lean"), c("ideology", "party affiliation")) %>% 
  pwalk(~grapher(pew, income, ..1, ..2))
```

![](ch2_files/figure-gfm/pew-1.png)<!-- -->![](ch2_files/figure-gfm/pew-2.png)<!-- -->

## Section 2.2

The authors point out that while we typically take “measurement” for
granted, it matters what we’re measuring and how we intend to draw
inferences from our measurements. The authors frame this in three ways:

  - **Validity**: how much a measure represents what we intend to
    measure. A valid measure should give the right answer on average,
    across a range of scenarios.
  - **Reliability**: how precise and stable a measure is. The intent
    here is to avoid variability due to random errors during the
    measurement process.
  - **Sample selection**: whether the data is a representative sample of
    the unmeasurable larger population. This idea incorporates selection
    and nonresponse bias.

## Section 2.3

All graphs are comparisons, and visualization is part of exploration,
modeling, inference and model building. Comparisons can be to zero, to
other graphs, to lines, and so on. The authors give several
visualization tips:

  - **Scatter plots** can incorporate up to five dimensions easily
    (*x*-axis, *y*-axis, symbol, size and colour), with two extra
    dimensions available in a grid-based **small multiples**
    presentation of scatter plots. The authors give an example of how
    such scatter plots can help find interaction effects (which are
    required whenever regression lines are non-parallel).
  - **Readability**: consider making graphs smaller, always add a
    caption, and avoid graphs made simply for the sake of convention.
    Never make a graph that you can’t talk about.
  - **Comparisons**: make the most important comparison clearest. Use
    position on a common scale for easy interpretation.
  - **Fitted models**: consider graphing both a fitted model and the
    original data on the same plot.

## Section 2.4

To motivate the importance of adjusting measurements, the authors
provide the example of a study that found an increase in all-cause
mortality for 45- to 54-year old white non-Hispanic Americans between
1999 and 2013. However, because the average age within that age band had
changed, and because the paper’s authors had not stratified by sex,
Gelman *et al.* were skeptical of the paper’s results.

``` r
age <- read.table("https://raw.githubusercontent.com/avehtari/ROS-Examples/master/AgePeriodCohort/data/white_nonhisp_death_rates_from_1999_to_2013_by_sex.txt",
                  header = T)

p1 <- age %>%
  filter(Age >= 45 & Age <= 54) %>% 
  group_by(Year) %>% 
  summarize(deaths = sum(Deaths),
            population = sum(Population),
            mortality = deaths / population) %>%
  ggplot() +
  geom_line(aes(Year, mortality, group = 1)) +
  theme_daniel() +
  theme(plot.title = element_text(size = 10)) +
  labs(title = stringr::str_wrap("Mortality among white non-Hispanic American 45-to-54-year-olds appears to be increasing", width = 40),
       x = "Year",
       y = "Death rate")

p1.1 <- age %>% 
  filter(Age >= 45 & Age <= 54) %>% 
  mutate(age_product = Age * Population) %>% 
  group_by(Year) %>% 
  summarize(average_age = sum(age_product) / sum(Population)) %>% 
  ggplot() +
  geom_line(aes(Year, average_age, group = 1)) +
  theme_daniel() +
  theme(plot.title = element_text(size = 10)) +
  labs(title = stringr::str_wrap("But the average age of this group increased over the time range in question", width = 40),
       x = "Year",
       y = "Average age")

adjust_age <- function(df, year_var, age_var, ...) {
  df %>% 
    filter(Age >= 45 & Age <= 54) %>% 
    group_by({{year_var}}, {{age_var}}, ...) %>% 
    summarize(deaths = sum(Deaths),
            population = sum(Population),
            mortality = deaths / population) %>%
    group_by({{year_var}}, ...) %>% 
    summarize(mean_mortality = mean(mortality))
}

p2 <- adjust_age(age, Year, Age) %>%
  ggplot() +
  geom_line(aes(Year, mean_mortality, group = 1)) +
  theme_daniel() +
  theme(plot.title = element_text(size = 10)) +
  labs(title = stringr::str_wrap("When age-adjusted, mean mortality stopped increasing in 2005", width = 40),
       x = "Year",
       y = "Age-adjusted death rate")

p3 <- age %>% 
  mutate(Male = case_when(Male == 0 ~ "Women",
                          T ~ "Men")) %>% 
  adjust_age(., Year, Age, Male) %>%
  ggplot() +
  geom_line(aes(Year, mean_mortality, group = Male, colour = Male), size = 1) +
  scale_colour_daniel(name = "Sex", palette = "main") +
  theme_daniel() +
  theme(plot.title = element_text(size = 10)) +
  labs(title = stringr::str_wrap("And age-adjusted mortality has increased more for women, proportionally speaking", width = 39),
       x = "Year",
       y = NULL)

(p1 | p1.1) /
(p2 | p3)
```

![](ch2_files/figure-gfm/ageperiodcohort-1.png)<!-- -->
