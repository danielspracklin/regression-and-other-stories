# regression-and-other-stories

I picked up a copy of Andrew Gelman, Jennifer Hill and Aki Vehtari's new book, [_Regression and Other Stories_](https://avehtari.github.io/ROS-Examples/). For fun, I'm going to work through the examples independently here, then cross-check against the [official source](https://avehtari.github.io/ROS-Examples/examples.html).
